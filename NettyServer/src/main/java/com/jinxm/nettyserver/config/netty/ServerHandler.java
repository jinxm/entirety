package com.jinxm.nettyserver.config.netty;


import com.jinxm.nettyserver.entity.MessageEntity;
import com.jinxm.nettyserver.repository.MessageRepository;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.util.Date;

@Component
@Qualifier("serverHandler")
@Sharable
public class ServerHandler extends SimpleChannelInboundHandler<String> {
    private static final Logger log = LoggerFactory.getLogger(ServerHandler.class);


    @Autowired
    private MessageRepository messageRepository;

    @Override
    protected void messageReceived(ChannelHandlerContext channelHandlerContext, String msg) throws Exception {
        log.info("client msg:" + msg);
        String clientIdToLong = channelHandlerContext.channel().id().asLongText();
        log.info("client long id:" + clientIdToLong);
        String clientIdToShort = channelHandlerContext.channel().id().asShortText();
        log.info("client short id:" + clientIdToShort);

        MessageEntity messageEntity = new MessageEntity(msg, clientIdToLong, clientIdToShort, new Date());

        messageRepository.save(messageEntity);
        if (msg.contains("bye")) {
            //close
            channelHandlerContext.channel().close();
        } else {
            //send to client
            channelHandlerContext.channel().writeAndFlush("Yor msg is:" + msg);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.info("\nChannel is disconnected");
        super.channelInactive(ctx);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("RemoteAddress : " + ctx.channel().remoteAddress() + " active !");
        ctx.channel().writeAndFlush("Welcome to " + InetAddress.getLocalHost().getHostName() + " service!\n");
        super.channelActive(ctx);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

}