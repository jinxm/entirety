package com.jinxm.nettyserver.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "message")
public class MessageEntity implements Serializable {

    private static final long serialVersionUID = -2036933492520755732L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String msg;

    private String idLongText;

    private String idShortText;

    private Date receivedDate;


    public MessageEntity(String msg, String idLongText, String idShortText, Date receivedDate) {
        this.msg = msg;
        this.idLongText = idLongText;
        this.idShortText = idShortText;
        this.receivedDate = receivedDate;
    }

    public MessageEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIdLongText() {
        return idLongText;
    }

    public void setIdLongText(String idLongText) {
        this.idLongText = idLongText;
    }

    public String getIdShortText() {
        return idShortText;
    }

    public void setIdShortText(String idShortText) {
        this.idShortText = idShortText;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }
}
