package com.jinxm.nettyserver.repository;

import com.jinxm.nettyserver.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MessageRepository extends JpaRepository<MessageEntity, Integer> {

}
